
public class TennisGame2 implements TennisGame
{
	//public static final int 1=LOVE;
	
    public int player1Points = 0;
    public int player2Points = 0; 
    
    public String player1Name;
    public String player2Name;
    
    public TennisGame2(String player1Name, String player2Name) {
    	this.player1Name = player1Name;
        this.player2Name = player2Name;
    }
    public String getLiteralScore(){
    	String literalScore ="";
    	if (isNormal())
        {
    		literalScore = getLiteral(player1Points) + "-" + getLiteral(player2Points);
        }
    	if (isTie())
        {
    		literalScore = getLiteral(player1Points) + "-All";
        }
    	if (isDeuce())
        {
    		literalScore = "Deuce";
        }
    	if (isAdvantage(player1Points,player2Points))
        {
    		literalScore = "Advantage player1";
        }
    	
    	if (isAdvantage(player2Points,player1Points))
        {
    		literalScore = "Advantage player2";
        }
    	
    	if (isWinner(player1Points,player2Points))
        {
    		literalScore = "Win for player1";
        }
    	
        if (isWinner(player2Points,player1Points))
        {
        	literalScore = "Win for player2";
        }
        return literalScore;
    }

	public boolean isNormal() {
		return player2Points!=player1Points;
	}

	public boolean isDeuce() {
		return player1Points==player2Points && player1Points>=3;
	}


	public boolean isTie() {
		return player1Points == player2Points && player1Points < 4;
	}
	
	public boolean isWinner(int firstPlayerPoints, int secondPlayerPoints) {
		return firstPlayerPoints>=4 && secondPlayerPoints>=0 && (firstPlayerPoints-secondPlayerPoints)>=2;
	}

	public boolean isAdvantage(int firstPlayerPoints, int secondPlayerPoints) {
		return firstPlayerPoints > secondPlayerPoints && secondPlayerPoints >= 3;
	}
    
    public void SetP1Score(int number){
        
        for (int i = 0; i < number; i++)
        {
            P1Score();
        }
            
    }
    
    public void SetP2Score(int number){
        
        for (int i = 0; i < number; i++)
        {
            P2Score();
        }
            
    }
    
    public void P1Score(){
        player1Points++;
    }
    
    public void P2Score(){
        player2Points++;
    }

    public void wonPoint(String player) {
        if (player == "player1")
            P1Score();
        else
            P2Score();
    }
    
    private String getLiteral(int playerPoints) {
    	String result="";
    	if (playerPoints==0)
    		result = "Love";
        if (playerPoints==1)
        	result = "Fifteen";
        if (playerPoints==2)
        	result = "Thirty";
        if (playerPoints==3)
        	result = "Forty";
        return result;
    	
    }
}